part of contacts_ex8;

class ContactModel extends ConceptModel {

  static final String contact = 'Contact';

  Map<String, ConceptEntities> newEntries() {
    var contacts = new Contacts();
    var map = new Map<String, ConceptEntities>();
    map[contact] = contacts;
    return map;
  }

  Contacts get contacts => getEntry(contact);

  init() {
    var contact1 = new Contact();
    contact1.code ='1';
    contact1.nom = 'Johnny Spring';
    contact1.email ='jspring@gmail.com';  
    contact1.phone = '418-999-999';
    contacts.add(contact1);

    var contact2 = new Contact();
    contact2.code ='2';
    contact2.nom = 'Joanne Summer';
    contact2.email ='josummer@gmail.com';  
    contact2.phone = '418-888-988';
    contacts.add(contact2);

    var contact3 = new Contact();
    contact3.code ='3';
    contact3.nom = 'Harvey Winter';
    contact3.email ='hwinter@gmail.com';  
    contact3.phone = '418-777-466';
    contacts.add(contact3);
  }

  display() {
    print('Contact Model');
    print('=============');
    contacts.display('Contacts');
    print(
      '============= ============= ============= '
      '============= ============= ============= '
    );
  }

}


