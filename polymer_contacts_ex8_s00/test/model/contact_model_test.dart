import 'package:unittest/unittest.dart';
import 'package:dartlero/dartlero.dart';
import 'package:polymer_contacts_ex8/contacts_ex8.dart';

testContacts(Contacts contacts) {
  group("Testing Contacts", () {
    setUp(() {
      var contactCount = 0;
      expect(contacts.length, equals(contactCount));

      var contact1 = new Contact();
      expect(contact1, isNotNull);
      contact1.nom = 'Jean Michel';
      contact1.email ='jean.michel@gmail.com';  
      contact1.phone = '418-999-999';
      contacts.add(contact1);
      expect(contacts.length, equals(++contactCount));
      
      
      var contact2 = new Contact();
      expect(contact2, isNotNull);
      contact2.nom = 'Marie Dupont';
      contact2.email ='mariedupont@gmail.com';  
      contact2.phone = '418-888-988';
      contacts.add(contact2);
      expect(contacts.length, equals(++contactCount));

      var contact3 = new Contact();
      contact3.nom = 'Julien Tremblay';
      contact3.email ='julientremblay@gmail.com';  
      contact3.phone = '418-777-466';
      contacts.add(contact3);
      expect(contacts.length, equals(++contactCount));
      
      contacts.display('Contacts');
    });
    
    tearDown(() {
      contacts.clear();
      expect(contacts.isEmpty, isTrue);
    });
    
    test('Add Contact', () {
      var contact = new Contact();
      expect(contact, isNotNull);
      contact.nom = 'Bechir Sellami';
      contact.email = 'bechir.sellami@gmail.com';
      contact.phone = '581-999-8934';
    
      var added = contacts.add(contact);
      expect(added, isTrue);
      contacts.display('Add Project');
    });
    
    
    test('Add Contact Without Data', () {
      var contactCount = contacts.length;
      var contact = new Contact();
      expect(contact, isNotNull);
      var added = contacts.add(contact);
      expect(added, isTrue);
      contacts.display('Add Contact Without Data');
    });
    
    test('Add Contact Not Unique', () {
      var contactCount = contacts.length;
      var contact = new Contact();
      expect(contact, isNotNull);
      contact.nom = 'Julien Tremblay';   
      var added = contacts.add(contact);
      expect(added, isFalse);
      contacts.display('Add Contact Not Unique');
    });
   
    test('Find Contact by Name', () {
      var searchName = 'Julien Tremblay';
      var contact = contacts.find(searchName);
      expect(contact, isNotNull);
      expect(contact.nom, equals(searchName));
      contact.display('Find Contact by Name');
    });
    
      test('Select Contacts by Phone Region', () {
      var RegionContacts = contacts.select((c) => c.onRegion);
      expect(RegionContacts.isEmpty, isFalse);
      expect(RegionContacts.length, equals(3));
      RegionContacts.display('Select Contacts by Phone Region');
    });
    
    test('Select Contacts by Region then Add', () {
      var RegionContacts = contacts.select((c) => c.onRegion);
      expect(RegionContacts.isEmpty, isFalse);

      var phoneTesting = 'Julie Doré';
      var phonecontact = new Contact();
      phonecontact.nom = phoneTesting;
      phonecontact.email = 'juliedoré@gmail.com';
      phonecontact.phone = '418-999-5678';
      
      var added = RegionContacts.add(phonecontact);
      expect(added, isTrue);
      RegionContacts.display('Select Contacts by Region then Add');

      var contact = contacts.find(phoneTesting);
      expect(contact, isNull);
      contacts.display('Contacts');
    });
   
    test('Select Contacts by Region then Remove', () {
      var contactCount = contacts.length;
      contacts.display('Contacts Before Remove');
      var RegionContacts = contacts.select((c) => c.onRegion);
      expect(RegionContacts.isEmpty, isFalse);

      var searchName = 'Jean Michel';
      var contact = RegionContacts.find(searchName);
      expect(contact, isNotNull);
      expect(contact.nom, equals(searchName));
      var regionContactCount = RegionContacts.length;
      RegionContacts.remove(contact);
      expect(RegionContacts.length, equals(--regionContactCount));
      expect(contacts.length, equals(contactCount));
    });
    
    
    test('Order Contacts by Name', () {
      contacts.orderByFunction((m,n) => m.compareTo(n));
      contacts.display('Order Contacts by Name');
    });
    
    
    test('New Contact', () {
      var contactCount = contacts.length;
      var contact4 = new Contact();
      expect(contact4, isNotNull);
      contact4.nom = 'Oscar Peterson';
      contact4.email = 'oscarpeterson@gmail.com';
      var added = contacts.add(contact4);
      expect(added, isTrue);
      expect(contacts.length, equals(++contactCount));
      contacts.display('New Contact');
    });
    test('Copy Contacts', () {
      Contacts copiedContacts = contacts.copy();
      expect(copiedContacts.isEmpty, isFalse);
      expect(copiedContacts.length, equals(contacts.length));
      expect(copiedContacts, isNot(same(contacts)));
      expect(copiedContacts, isNot(equals(contacts)));
      copiedContacts.forEach((cp) =>
          expect(cp, isNot(same(contacts.find(cp.nom)))));
      copiedContacts.display('Copied Contacts');
     //projects.display('Projects');
    });

    test('True for Every Contact', () {
      expect(contacts.every((p) => p.code != null), isTrue);
      expect(contacts.every((p) => p.nom != null), isTrue);
    });
    
    
    test('From Contacts to JSON', () {
      var json = contacts.toJson();
      expect(json, isNotNull);
      print(json);
    });
    test('From JSON to Contact Model', () {
      List<Map<String, Object>> json = contacts.toJson();
      contacts.clear();
      expect(contacts.isEmpty, isTrue);
      contacts.fromJson(json);
      expect(contacts.isEmpty, isFalse);
      contacts.display('From JSON to Projects');
    });
  });
}

initDisplayModel() {
  ContactModel contactModel = new ContactModel();
  contactModel.init();
  contactModel.display();
}

testModel() {
  ContactModel contactModel = new ContactModel();
  Contacts contacts = contactModel.contacts;
  testContacts(contacts);
}

main() {
  //initDisplayModel();
  testModel();
}
