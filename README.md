
# polymer_contacts_ex8 : Application with dartlero and Polymer.dart: Spirals

**Author**: Bechir Sellami, NI : 111 052 979

**Categories**: homework, SIO 6014 : Applications Web des SIO

**Concepts**: contact.

**Description**:
Using polymer components on a model with one concept

**References**: 
polymer_category_links web application : https://github.com/dzenanr/polymer_category_links
