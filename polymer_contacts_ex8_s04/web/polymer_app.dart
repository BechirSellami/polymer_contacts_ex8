import 'dart:html';
import 'dart:convert';
import 'package:polymer_contacts_ex8/contacts_ex8.dart';
import 'package:polymer/polymer.dart';

@CustomTag('polymer-app')
class PolymerApp extends PolymerElement {
  @observable Contacts contacts;

  PolymerApp.created() : super.created() {
    var contactModel = new ContactModel();
    contacts = contactModel.contacts;

    // load data
    String json = window.localStorage['polymer_contact_ex8'];
    if (json == null) {
      contactModel.init();
    } else {
      contacts.fromJson(JSON.decode(json));
    }

    contacts.internalList = toObservable(contacts.internalList);
  }

  save(Event e, var detail, Node target) {
    window.localStorage['polymer_contact_ex8'] =
        JSON.encode(contacts.toJson());
  }
}