import 'dart:html';
import 'package:polymer_contacts_ex8/contacts_ex8.dart';
import 'package:polymer/polymer.dart';
import 'contact_table.dart';

@CustomTag('contact-edit')
class ContactEdit extends PolymerElement {
  @published Contacts contacts;
  @published Contact contact;
  @published String name;
  @published String email;
  @published String phone;
  ContactEdit.created() : super.created();

  enteredView() {
    super.enteredView();
    name = contact.nom;
    email = contact.email;
    phone = contact.phone;
  }

  update(Event e, var detail, Node target) {
    
    contact.nom = name;
    contact.email = email;
    contact.phone = phone;
    
    contacts.order(); 
    var polymerApp = querySelector('#polymer-app');
    ContactTable contactTable = polymerApp.shadowRoot.querySelector('#contact-table');
    contactTable.showEdit = false;
  }
}