import 'dart:html';
import 'package:polymer_contacts_ex8/contacts_ex8.dart';
import 'package:polymer/polymer.dart';

@CustomTag('contact-table')
class ContactTable extends PolymerElement {
  @published Contacts contacts;
  @observable bool showAdd = false;

  ContactTable.created() : super.created();
  
  show(Event e, var detail, Node target) {
    ButtonElement addContact = shadowRoot.querySelector("#show-add");
    if (addContact.text == 'Show Add') {
      showAdd = true;
      addContact.text = 'Hide Add';
    } else {
      showAdd = false;
      addContact.text = 'Show Add';
    }
  }
}