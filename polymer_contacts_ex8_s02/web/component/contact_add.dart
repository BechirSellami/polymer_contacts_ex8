import 'dart:html';
import 'package:polymer_contacts_ex8/contacts_ex8.dart';
import 'package:polymer/polymer.dart';

@CustomTag('contact-add')
class ContactAdd extends PolymerElement {
  @published Contacts contacts;

  ContactAdd.created() : super.created();

  add(Event e, var detail, Node target) {
    InputElement name = shadowRoot.querySelector("#name");
    InputElement email = shadowRoot.querySelector("#email");
    InputElement phone = shadowRoot.querySelector("#phone");
    Element message = shadowRoot.querySelector("#message");
    var error = false;
    message.text = '';
    if (name.value.trim() == '') {
      message.text = 'contact name is mandatory; ${message.text}';
      error = true;
    }
    if (!error) {
      var contact = new Contact();
      contact.nom = name.value;
      contact.email = email.value;
      contact.phone = phone.value;
      if (contacts.add(contact)) {
        message.text = 'added';
        contacts.order();
      } else {
        message.text = 'name already in use';
      }
    }
  }
}