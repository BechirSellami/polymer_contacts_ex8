import 'package:polymer_contacts_ex8/contacts_ex8.dart';
import 'package:polymer/polymer.dart';

@CustomTag('contact-table')
class ContactTable extends PolymerElement {
  @published Contacts contacts;

  ContactTable.created() : super.created();
}