import 'package:polymer_contacts_ex8/contacts_ex8.dart';
import 'package:polymer/polymer.dart';

@CustomTag('polymer-app')
class PolymerApp extends PolymerElement {
  @observable Contacts contacts;

  PolymerApp.created() : super.created() {
    var contactModel = new ContactModel();
    contactModel.init();
    contacts = contactModel.contacts;
  }
}